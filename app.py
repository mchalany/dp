import json
import os

import tkinter as tk
from tkinter import messagebox
from tkinter import filedialog

from modules.frames import home, parameters, cipher, measure
from modules.components import keys
from modules.components.salomaa_welzl import SalomaaWelzl
import settings


class App(tk.Tk):

    def __init__(self):

        tk.Tk.__init__(self)
        self.title(settings.APP_TITLE)
        self.configure(bg=settings.BG)
        self.resizable(False, False)
        self.geometry("350x350")

        self.salomaa_welzl = SalomaaWelzl(keys.PrivateKey(), keys.PublicKey())
        with open(settings.PRIVATE_KEY_PATH, 'r') as key_file:
            self.salomaa_welzl.private.load_from_dict(json.load(key_file))
        with open(settings.PUBLIC_KEY_PATH, 'r') as key_file:
            self.salomaa_welzl.public.load_from_dict(json.load(key_file))

        with open('misc/help.json', 'r') as f:
            self.help = json.load(f)

        self.init_menu()

        self._frame = tk.Frame(self)
        self._frame.place(x=75, y=150)
        tk.Message(self._frame, width=220, text="Start by setting parameters, "
                   "loading keys and choosing action from menu!",
                   bg=settings.BG, fg=settings.FG).pack()
        self.active_frame = 'home'

    def init_menu(self):
        self.menubar = tk.Menu(self, fg=settings.FG, bg=settings.BG)

        self.start = tk.Menu(
            self.menubar, fg=settings.FG, bg=settings.BG, tearoff=0)
        self.start.add_command(label="Attack",
                               command=lambda: self.check_action('attack'))
        self.start.add_command(label="Cipher",
                               command=lambda: self.check_action('cipher'))
        self.start.add_command(label="Decipher",
                               command=lambda: self.check_action('decipher'))
        self.start.add_command(label="Measure",
                               command=lambda: self.check_action('measure'))
        self.menubar.add_cascade(label="Action", menu=self.start)

        self.parameters = tk.Menu(self.menubar, fg=settings.FG,
                                  bg=settings.BG, tearoff=0)
        self.parameters.add_command(label="Load private key",
                                    command=lambda: self.keyload('g'))
        self.parameters.add_command(label="Load public key",
                                    command=lambda: self.keyload('h'))
        self.parameters.add_command(label="Change keys",
                                    command=lambda: self.switch_frame(
                                        parameters.ParametersFrame))
        self.parameters.add_command(label="Export keys",
                                    command=self.export)
        self.menubar.add_cascade(label="Parameters", menu=self.parameters)

        self.menubar.add_command(label="Help",
                                 command=lambda: self.show_help())
        self.menubar.add_command(label="About",
                                 command=lambda: self.show_about())

        self.config(menu=self.menubar)

    def show_help(self):
        sprava = self.help[self.active_frame]
        messagebox.showinfo('Help', sprava)

    def show_about(self):
        messagebox.showinfo('About', 'Autor: Michal Calány \n'
                            'Diploma project - school year 2018/2019')

    def check_action(self, action):
        if not (self.salomaa_welzl.public.alphabet and
                self.salomaa_welzl.public.substitutions and
                self.salomaa_welzl.public.word):
            check_public = False
        else:
            check_public = True
        if not (self.salomaa_welzl.private.alphabet and
                self.salomaa_welzl.private.morphizms and
                self.salomaa_welzl.private.function and
                self.salomaa_welzl.private.word):
            check_private = False
        else:
            check_private = True
        if action == 'cipher':
            if not check_public:
                messagebox.showerror('Error', 'Incorrect public key, cannot encrypt!')
                return
            else:
                self.switch_frame(cipher.CipherFrame, 'c')
        elif action == 'decipher':
            if not check_private:
                messagebox.showerror('Error', 'Incorrect private key, cannot decrypt!')
                return
            else:
                self.switch_frame(cipher.CipherFrame, 'd')
        elif action == 'attack':
            if not check_public:
                messagebox.showerror('Error', 'Incorrect public key, cannot use cryptanalysis!')
                return
            else:
                self.switch_frame(home.HomeFrame)
        elif action == 'measure':
            if not (check_private and check_public):
                messagebox.showerror('Error', 'Cannot measure, at least one key missing!')
                return
            else:
                self.switch_frame(measure.MeasureFrame)

    def switch_frame(self, frame_class, data=None):
        if data is None:
            new_frame = frame_class(self)
        else:
            new_frame = frame_class(self, data)
        if self.active_frame == 'settings':
            if frame_class.__name__ in ['CipherFrame',
                                        'HomeFrame',
                                        'DecipherFrame']:
                if (self.salomaa_welzl.private.is_complete and
                        self.salomaa_welzl.public.is_complete):
                    if self._frame is not None:
                        self._frame.destroy()
                    self._frame = new_frame
                    self._frame.pack()
                else:
                    messagebox.showerror('Error',
                                         'All parameters must be set!')
                    return

        if self._frame is not None:
            self._frame.destroy()
        self._frame = new_frame
        self._frame.pack()

    def keyload(self, key_type):
        dialog = filedialog.Open(self, filetypes=(("all files", "*.*"),))
        fl = dialog.show()
        if fl != '':
            try:
                with open(fl, 'r') as f:
                    if key_type == 'g':
                        data = json.load(f)
                        self.salomaa_welzl.private.load_from_dict(data)
                    elif key_type == 'h':
                        data = json.load(f)
                        self.salomaa_welzl.public.load_from_dict(data)
                messagebox.showinfo('Success', 'Key successfully loaded!')
                if self.active_frame == 'main':
                    self.switch_frame(parameters.ParametersFrame)
            except Exception:
                messagebox.showerror('Error', 'Key load not completed!')
                return

    def check_parameters(self):
        for key in (self.salomaa_welzl.private, self.salomaa_welzl.public):
            members = [attr for attr in dir(key)
                       if not callable(getattr(key, attr))
                       and not attr.startswith("_")
                       and attr not in ['morph_count',
                                        'subst_count',
                                        'char_count']]
            for m in members:
                if not getattr(key, m):
                    return False
        return True

    # check ci uz existuje dana dvojica klucov, ak hej vrati cislo, inak False
    def check_existing_keys(self):
        folders = [x[0] for x in os.walk('keys/') if x[0] != 'keys/']
        for folder in folders:
            tmp_private = folder + '/private.json'
            tmp_public = folder + '/public.json'
            with open(tmp_private, 'r') as tpr:
                data_private = json.load(tpr)
            with open(tmp_public, 'r') as tp:
                data_public = json.load(tp)
            if self.salomaa_welzl.private.to_dict() == data_private and \
                    self.salomaa_welzl.public.to_dict() == data_public:
                return int(folder[5:])
        return False

    def export(self):
        if not self.check_parameters():
            messagebox.showerror('Error', 'Check parameters!')
            return
        if self.check_existing_keys():
            messagebox.showerror('Error', 'Existing key, (number {})!'.format(
                                 self.check_existing_keys()))
            return
        if not os.path.isdir('keys'):
            os.mkdir('keys')
        if not os.path.isdir('keys/1'):
            os.mkdir('keys/1')
            private_keyfile = 'keys/1/private.json'
            public_keyfile = 'keys/1/public.json'
        else:
            folder = str(max([int(x[0][5:]) for x in os.walk('keys/')
                         if x[0] != 'keys/']) + 1)
            os.mkdir('keys/' + folder)
            private_keyfile = 'keys/' + folder + '/private.json'
            public_keyfile = 'keys/' + folder + '/public.json'
        with open(private_keyfile, 'w') as f:
            json.dump(self.salomaa_welzl.private.to_dict(), f)
        with open(public_keyfile, 'w') as f:
            json.dump(self.salomaa_welzl.public.to_dict(), f)

        messagebox.showinfo('Success', 'Export successful!')


app = App()
app.mainloop()
