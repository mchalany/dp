# README

Desktop application for Diploma project - [SalomaaWelzl cryptosystem](https://www.sciencedirect.com/science/article/pii/030439758690099X) implementation (2019)

Application consists of:

- definition of custom parameters for SalomaaWelzl cryptosystem instance
- encrypting text with created cryptosystem instance
- decrypting encrypted text
- attacking the encrypted text with known cryptanalysis attack

![dp](./dp.gif)
