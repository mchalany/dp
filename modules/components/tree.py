class Node:
    def __init__(self):
        self.data = None
        self.sigma = None
        self.children = []
        self.parent = None
        self.kids = 0
        self.circle_pos = ()
        self.line_from = ()
        self.line_to = ()
        self.circle = None
        self.fill = "blue"

    def add_child(self, child):
        self.children.append(child)
        self.kids += 1

    def print_children(self):
        for c in self._children:
            print(c.data)

    def get_ancestors(self):
        result = []
        node = self
        while node.parent is not None:
            result.insert(0, node.parent.data)
            node = node.parent
        return result

    def get_sigmas(self):
        result = [self.sigma]
        node = self
        while node.parent is not None:
            result.insert(0, node.parent.sigma)
            node = node.parent
        return result[::-1]

    def get_levels(self, levels, level):
        if level == 0:
            levels[level] = [self]
        elif level not in levels:
            levels[level] = [self]
        else:
            levels[level].append(self)
        for x in self.children:
            x.get_levels(levels, level + 1)
        width = 0
        for l in levels.values():
            if len(l) > width:
                width = len(l)
        height = len(levels)
        return levels, width, height

    def find_node(self, data, node='node'):
        if node == 'node':
            node = self
        if node.data == data:
            return node
        if node.children:
            for i, x in enumerate(node.children):
                if self.find_node(data, x) is not None:
                    return self.find_node(data, x)

    def render(self, r, canvas, node=0):
        if node == 0:
            node = self
        if node is None:
            return
        if node.parent is not None:
            node.line = canvas.create_line(node.parent.line_from, node.line_to)
        node.circle = canvas.create_oval(
            node.circle_pos,
            node.circle_pos[0] + r,
            node.circle_pos[1] + r,
            fill=node.fill,
            outline=node.fill)
        tmp = canvas.bbox("all")
        canvas.config(scrollregion=tmp)
        for x in node.children:
            self.render(r, canvas, x)
