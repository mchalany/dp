class Key:

    def __init__(self, alphabet=[], word=[]):
        self.alphabet = alphabet
        self.word = word
        self.char_count = 0

    @property
    def word(self):
        return self._word

    @word.setter
    def word(self, word):
        self._word = word
        self.char_count = len(word)

    def to_dict(self):
        export = {}
        members = [attr for attr in dir(self)
                   if not callable(getattr(self, attr))
                   and not attr.startswith("_")
                   and attr not in ['morph_count',
                                    'subst_count',
                                    'char_count']]
        for m in members:
            export[m] = getattr(self, m)
        return export

    def load_from_dict(self, data):
        self.alphabet = data['alphabet']
        self.word = data['word']

    def load_key(self, key):
        self.alphabet = key.alphabet
        self.word = key.word

    def is_complete(self):
        if self.alphabet and self.word:
            return True
        else:
            return False


class PrivateKey(Key):

    def __init__(self, alphabet=[], word=[], morphizms={}, function={}):
        super().__init__(alphabet, word)
        self.function = function
        self._morphizms = morphizms
        self.morph_count = 0

    @property
    def morphizms(self):
        return self._morphizms

    @morphizms.setter
    def morphizms(self, morphizms):
        self._morphizms = morphizms
        self.morph_count = len(morphizms)

    def items_dict_to_function(self, items_dict):
        function = {}
        for key, value in items_dict.items():
            for v in value:
                function[str(v)] = key
        self.function = function

    def analyse_function(self):
        items_dict = {}
        for char in self.alphabet:
            items_dict[str(char)] = []
        items_dict['e'] = []
        for key, value in self.function.items():
            items_dict[str(value)].append(int(key))
        return items_dict

    def load_from_dict(self, data):
        super().load_from_dict(data)
        self.morphizms = data['morphizms']
        self.function = data['function']

    def load_key(self, key):
        super().load_key(key)
        self.morphizms = key.morphizms
        self.function = key.function

    def is_complete(self):
        if super().is_complete():
            if self.morphizms and self.function:
                return True
        return False


class PublicKey(Key):

    def __init__(self, alphabet=[], word=[], substitutions={}):
        super().__init__(alphabet, word)
        self._substitutions = substitutions
        self.subst_count = 0

    @property
    def substitutions(self):
        return self._substitutions

    @substitutions.setter
    def substitutions(self, substitutions):
        self._substitutions = substitutions
        self.subst_count = len(substitutions)

    def load_from_dict(self, data):
        super().load_from_dict(data)
        self.substitutions = data['substitutions']

    def load_key(self, key):
        super().load_key(key)
        self.substitutions = key.substitutions

    def is_complete(self):
        if super().is_complete():
            if self.substitutions:
                return True
        return False
