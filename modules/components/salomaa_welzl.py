from tkinter import messagebox
import random

from modules.components import tree

from utils.helpers import get_last_value, get_delta, get_graph


class SalomaaWelzl:

    def __init__(self, private=None, public=None):
        self.private = private
        self.public = public

    def encrypt(self, bits, word=None):
        '''Enkrypcia pomocou verejneho kluca
        '''
        if word is None:
            word = self.public.word
        if not bits:
            return word
        s = []
        for u in word:
            tmp = random.choice(self.public.substitutions[int(bits[0])][u])
            s.extend(tmp)
        return self.encrypt(bits[1:], s)

    def decrypt(self, ct):
        '''Dekrypcia pomocou sukromneho kluca
        '''
        ct = [self.private.function[z] for z in ct.split(',')\
              if self.private.function[z] != "e"]
        if ct:
            return self.recurse_decrypt(ct)
        else:
            messagebox.showerror('Chyba', 'Šifrovaný text tvorený len prázdnymi symbolmi!')

    def recurse_decrypt(self, ct):
        '''Rekurzia pouzita pri dekrypcii
        '''
        if ct[-1] in [x[-1] for x in self.private.morphizms[0]]:
            bit = 0
        else:
            bit = 1
        result = []
        while ct != []:
            last = get_last_value(ct, self.private.morphizms[bit])
            result.append(self.private.morphizms[bit].index(last))
            ct = ct[:-len(last)]
        if result[::-1] == self.private.word:
            return bit
        else:
            return str(self.recurse_decrypt(result[::-1])) + str(bit)

    def attack(self, ct, parent=None):
        node = tree.Node()
        node.parent = parent
        node.data = ct
        if ct == self.public.word:
            return node

        node.sigma = 0
        delta = get_delta(ct, self.public.substitutions[0])
        if delta:
            _list = get_graph(delta, [])
        if not _list or _list is None:
            node.sigma = 1
            delta = get_delta(ct, self.public.substitutions[1])
            if delta:
                _list = get_graph(delta, [])
        if _list or _list is not None:
            for i, z in enumerate(_list):
                node.add_child(self.attack(z, node))
        else:
            return node
        return node
