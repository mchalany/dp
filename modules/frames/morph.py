import tkinter as tk
from tkinter import messagebox

from modules.frames import parameters


class MorphFrame(tk.Frame):

    def __init__(self, master, parent):
        tk.Frame.__init__(self)
        self.parent = parent
        self.private = parent.curr_private
        self.public = parent.curr_public
        self.check_morph_count = 0
        self.init_components()

    def init_components(self):
        self.textvars = []
        self.entries = []
        self.grid_rowconfigure(1, weight=1)
        self.grid_rowconfigure(2, weight=1)
        for p in range(self.private.char_count):
            self.grid_rowconfigure(p, weight=1)
            tk.Label(self, text=p).grid(row=p, column=1)
            self.textvars.append(tk.StringVar())
            self.entries.append(tk.Entry(self, textvariable=self.textvars[-1]))
            self.entries[-1].grid(row=p, column=2, columnspan=2, sticky='wse')
            tk.Button(self, text="Next", command=lambda: self.next())\
                .grid(sticky='wse', row=p+1, column=2, columnspan=2)
        self.init_model(self.check_morph_count)

    def init_model(self, current):
        if len(self.private.morphizms) - 1 >= current:
            for i, value in enumerate(self.private.morphizms[current]):
                value = ','.join([str(x) for x in value])
                self.textvars[int(i)].set(value)

    def next(self):
        morphizm = []
        for p in range(self.private.char_count):
            morphizm.append([int(x) for x in self.textvars[p].get().split(',')
                            if x != ''])
            self.entries[p].delete(0, tk.END)
        if len(self.private.morphizms) - 1 > self.check_morph_count:
            self.private.morphizms[self.check_morph_count] = morphizm
        else:
            self.private.morphizms.append(morphizm)
        self.check_morph_count += 1
        self.init_model(self.check_morph_count)
        if self.check_morph_count == self.private.morph_count:
            messagebox.showinfo('Success',
                                'All morphizms successfully created!')
            data = [self.private, self.public]
            self.master.switch_frame(parameters.ParametersFrame, data)
