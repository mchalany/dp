import tkinter as tk
from tkinter import messagebox
import random

from modules.components import keys
from modules.frames import function, morph, subst

import settings


class ParametersFrame(tk.Frame):

    def __init__(self, master, data=None):
        tk.Frame.__init__(self, master)
        self.master = master
        self.master.active_frame = 'settings'

        self.salomaa_welzl = master.salomaa_welzl

        self.curr_private = keys.PrivateKey()
        self.curr_public = keys.PublicKey()

        if not data:
            self.curr_private.load_key(self.salomaa_welzl.private)
            self.curr_public.load_key(self.salomaa_welzl.public)
        else:
            self.curr_private = data[0]
            self.curr_public = data[1]

        self.init_components()

    def init_components(self):
        self.master.geometry("350x350")
        group_g = tk.LabelFrame(self, text="Private key",
                                bg=settings.BG, fg=settings.FG)
        group_g.grid(row=1, column=1, sticky='WE')
        group_h = tk.LabelFrame(self, text="Public key",
                                bg=settings.BG, fg=settings.FG)
        group_h.grid(row=2, column=1, sticky='WE')
        buttons = tk.LabelFrame(self, bg=settings.BG, fg=settings.FG)
        buttons.grid(row=3, column=1, sticky='WE', pady=(20, 0))

        tk.Label(group_g, text="Count of characters:",
                 bg=settings.BG, fg=settings.FG) \
            .grid(row=1, column=1, sticky='W')
        self.g_card = tk.StringVar()
        self.g_card_entry = tk.Entry(group_g, textvariable=self.g_card)
        self.g_card_entry.grid(row=1, column=2, columnspan=2, sticky='WE')

        tk.Label(group_g, text="Count of morphizms:",
                 bg=settings.BG, fg=settings.FG) \
            .grid(row=2, column=1, sticky='W')
        self.morf_numb = tk.StringVar()
        self.morf_numb_entry = tk.Entry(group_g, textvariable=self.morf_numb)
        self.morf_numb_entry.grid(row=2, column=2, columnspan=2, sticky='WE')

        tk.Label(group_g, text="Morphizms",
                 bg=settings.BG, fg=settings.FG) \
            .grid(row=3, column=1, sticky='W')
        tk.Button(group_g, text="Create",
                  command=lambda: self.create_morphism()) \
            .grid(row=3, column=2, sticky='W')
        tk.Button(group_g, text="Generate",
                  command=lambda: self.generate_morphism()) \
            .grid(row=3, column=3)

        tk.Label(group_g, text="Starting word:",
                 bg=settings.BG, fg=settings.FG) \
            .grid(row=4, column=1, sticky='W')
        self.g_word = tk.StringVar()
        self.g_word_entry = tk.Entry(group_g, textvariable=self.g_word)
        self.g_word_entry.grid(row=4, column=2, columnspan=2, sticky='WE')

        tk.Label(group_g, text="Application function",
                 bg=settings.BG, fg=settings.FG) \
            .grid(row=5, column=1, sticky='W')
        tk.Button(group_g, text="Create",
                  command=lambda: self.create_function()) \
            .grid(row=5, column=2, sticky='W')
        tk.Button(group_g, text="Generate",
                  command=lambda: self.generate_function()) \
            .grid(row=5, column=3, sticky='W')

        tk.Label(group_h, text="Count of characters:",
                 bg=settings.BG, fg=settings.FG) \
            .grid(row=1, column=1, sticky='W')
        self.h_card = tk.StringVar()
        self.h_card_entry = tk.Entry(group_h, textvariable=self.h_card)
        self.h_card_entry.grid(row=1, column=2, columnspan=2, sticky='WE')

        tk.Label(group_h, text="Substitutions",
                 bg=settings.BG, fg=settings.FG) \
            .grid(row=2, column=1, sticky='W')
        tk.Button(group_h, text="Create",
                  command=lambda: self.create_subst()) \
            .grid(row=2, column=2, sticky='W')
        tk.Button(group_h, text="Generate",
                  command=lambda: self.generate_subst()) \
            .grid(row=2, column=3)

        tk.Label(group_h, text="Starting word:",
                 bg=settings.BG, fg=settings.FG) \
            .grid(row=3, column=1, sticky='W')
        self.h_word = tk.StringVar()
        self.h_word_entry = tk.Entry(group_h, textvariable=self.h_word)
        self.h_word_entry.grid(row=3, column=2, columnspan=2, sticky='WE')

        tk.Button(buttons, text="Apply parameters",
                  command=lambda: self.apply_settings()) \
            .grid(row=1, column=1, sticky='WE', padx=0, pady=(15, 15))
        tk.Button(buttons, text="Clear parameters",
                  command=lambda: self.remove_settings()) \
            .grid(row=1, column=2, sticky='WE', padx=0, pady=(15, 15))

        self.init_model()
        self.configure(bg=settings.BG)

    def init_model(self):
        self.g_card.set(self.curr_private.char_count)
        self.h_card.set(self.curr_public.char_count)
        self.morf_numb.set(self.curr_private.morph_count)
        if self.curr_private.word:
            self.g_word.set(','.join(str(s) for s in self.curr_private.word))
        if self.curr_public.word:
            self.h_word.set(','.join(str(s) for s in self.curr_public.word))

    def create_word(self):
        word = []
        prvky = self.salomaa_welzl.private.analyse_function()
        for s in self.salomaa_welzl.private.word:
            word.append(random.choice(prvky[str(s)]))
        return word

    def apply_settings(self):
        if not (self.g_card.get() and
                self.h_card.get() and
                self.morf_numb.get() and
                self.g_word.get() and
                self.curr_private.morphizms and
                self.curr_private.function and
                self.curr_public.substitutions):
            messagebox.showerror('Error', 'Nevyplnený niektorý z parametrov!')
            return
        self.salomaa_welzl.private.load_key(self.curr_private)
        self.salomaa_welzl.public.load_key(self.curr_public)

        self.salomaa_welzl.private.abeceda = \
            [x for x in range(int(self.g_card.get()))]
        self.salomaa_welzl.private.word = \
            [int(x) for x in self.g_word.get().split(',')]
        self.salomaa_welzl.public.abeceda = \
            [x for x in range(int(self.h_card.get()))]

        if not self.h_word.get():
            self.salomaa_welzl.public.word = self.create_word()
        else:
            tmp = [int(x) for x in self.h_word.get().split(',')]
            if self.check_word(tmp):
                print(tmp)
                self.salomaa_welzl.public.word = tmp
            else:
                messagebox.showerror(
                    'Error', 'Zadané slovo verejného kľúča nie je možné'
                    'vytvoriť zo slova privátneho kľúča!')
                return
        messagebox.showinfo('Success', 'Kľúče nastavené!')
        self.master.switch_frame(ParametersFrame)

    def check_word(self, word):
        prvky = self.salomaa_welzl.private.analyse_function()
        word = [s for s in word
                if self.salomaa_welzl.private.function[str(s)] != 'e']
        if len(word) != len(self.salomaa_welzl.private.word):
            return False
        for i, w in enumerate(self.salomaa_welzl.private.word):
            if not word[i] in prvky[str(w)]:
                return False
        return True

    def remove_settings(self):
        self.g_card.set(0)
        self.h_card.set(0)
        self.morf_numb.set(0)
        self.g_word.set('')
        self.h_word.set('')
        self.curr_private = keys.PrivateKey()
        self.curr_public = keys.PublicKey()

    def create_function(self):
        if self.h_card.get() == '0':
            messagebox.showerror(
                'Error',
                'Nie je zadaný počet písmen abecedy verejného kľúča!')
            return
        elif self.g_card.get() == '0':
            messagebox.showerror(
                'Error',
                'Nie je zadaný počet písmen abecedy privátneho kľúča!')
            return

        count_g = int(self.g_card.get())
        count_h = int(self.h_card.get())
        morph_count = int(self.morf_numb.get())
        self.curr_private.char_count = count_g
        if self.morf_numb:
            self.curr_private.morph_count = morph_count
            self.curr_public.subst_count = morph_count
        self.curr_public.char_count = count_h
        if self.g_word.get():
            self.curr_private.word = \
                [int(x) for x in self.g_word.get().split(',')]
        if self.h_word.get():
            self.curr_public.word = \
                [int(x) for x in self.h_word.get().split(',')]
        self.master.switch_frame(function.FunctionFrame, self)

    def generate_function(self):
        if self.h_card.get() == '':
            messagebox.showerror(
                'Error',
                'Nie je zadaný počet písmen abecedy verejného kľúča!')
        elif self.g_card.get() == '':
            messagebox.showerror(
                'Error',
                'Nie je zadaný počet písmen abecedy privátneho kľúča!')
        else:
            self.curr_private.abeceda = \
                [x for x in range(int(self.g_card.get()))]
            prvky = {}
            count_g = int(self.g_card.get())
            count_h = int(self.h_card.get())

            epsilon = [x for x in range(count_g)]
            epsilon.append('e')

            delta = [x for x in range(count_h)]

            for e in epsilon:
                prvky[e] = []

            temp = [x for x in range(count_g)]
            temp.append('e')

            for e in epsilon:
                if epsilon[-1] == e:
                    prvky[e] = delta
                else:
                    poc = random.randint(1, len(delta) - len(temp))
                    for p in range(poc):
                        prvok = random.choice(delta)
                        prvky[e].append(prvok)
                        delta.remove(prvok)
                temp.remove(e)
            self.curr_private.items_dict_to_function(prvky)
            messagebox.showinfo('Success', 'Úspešne vygenerovaná funkcia fg!')

    def create_morphism(self):
        if not self.g_card.get():
            messagebox.showerror(
                'Error',
                'Nie je zadaný počet písmen abecedy privátneho kľúča!')
            return
        count_g = int(self.g_card.get())
        count_h = int(self.h_card.get())
        morph_count = int(self.morf_numb.get())
        self.curr_private.char_count = count_g
        if self.morf_numb:
            self.curr_private.morph_count = morph_count
            self.curr_public.subst_count = morph_count
        self.curr_public.char_count = count_h
        if self.g_word.get():
            self.curr_private.word = \
                [int(x) for x in self.g_word.get().split(',')]
        if self.h_word.get():
            self.curr_public.word = \
                [int(x) for x in self.h_word.get().split(',')]
        self.master.switch_frame(morph.MorphFrame, self)

    def check_image(self, max, count, morphizm, current):
        if not morphizm and not self.curr_private.morphizms:
            return True
        for z in morphizm:
            if z == current:
                return False
        return True

    def gen_image(self, curr_count, used, max_count, count, morphizm):
        image = []
        possible_values = list(range(0, count))
        for i in range(settings.PSO):
            if not possible_values:
                continue
            if i == 0:
                if curr_count > max_count:
                    first_possible_values = [x for x in possible_values
                                             if x in used[-max_count:]]
                else:
                    first_possible_values = [x for x in possible_values
                                             if x not in used[:-curr_count]]
                tmp = random.choice(first_possible_values)
            else:
                tmp = random.choice(possible_values)
            image.insert(0, tmp)
        if self.check_image(max_count, count, morphizm, image):
            used.append(image[-1])
            curr_count += 1
            return image, used, curr_count
        else:
            return self.gen_image(
                curr_count, used, max_count, count, morphizm)

    def gen_morphizm(self, char_count, char_counts, used_chars):
        morphizm = []
        curr_count = 1
        for p in range(char_count):
            image, used_chars, curr_count = self.gen_image(
                curr_count, used_chars, char_counts, char_count, morphizm)
            morphizm.append(image)
        return morphizm

    def find_char_counts(self, char_count, morph_count):
        counts = []
        if char_count < morph_count:
            messagebox.showerror(
                'Error', 'Na splnenie spätého determinizmu musí byť'
                'počet znakov väčší ako počet zobrazení!')
            return
        if char_count == morph_count:
            for i in range(char_count):
                counts.append(1)
        else:
            while char_count > 0:
                counts.append(round(char_count / morph_count))
                char_count -= counts[-1]
                morph_count -= 1
        return counts

    def generate_morphism(self):
        self.curr_private.morphizms = []
        if self.g_card.get() == '' or self.morf_numb.get() == '':
            return
        char_count = int(self.g_card.get())
        morph_count = int(self.morf_numb.get())
        char_counts = self.find_char_counts(char_count, morph_count)
        if not char_counts:
            return
        used_chars = []
        for count in range(morph_count):
            morphizm = self.gen_morphizm(
                char_count, char_counts[count], used_chars)
            self.curr_private.morphizms.append(morphizm)
        messagebox.showinfo('Success', 'Zobrazenia úspešne vygenerované!')

    def create_subst(self):
        if self.h_card.get() == '':
            messagebox.showerror(
                'Error', 'Nie je zadaný počet písmen abecedy verejného kľúča!')
            return
        count_g = int(self.g_card.get())
        count_h = int(self.h_card.get())
        morph_count = int(self.morf_numb.get())
        self.curr_private.char_count = count_g
        if self.morf_numb:
            self.curr_private.morph_count = morph_count
            self.curr_public.subst_count = morph_count
        self.curr_public.char_count = count_h
        if self.g_word.get():
            self.curr_private.word = \
                [int(x) for x in self.g_word.get().split(',')]
        if self.h_word.get():
            self.curr_public.word = \
                [int(x) for x in self.h_word.get().split(',')]
        self.master.switch_frame(subst.SubstFrame, self)

    def gen_subst(self, p, d, prvky, used):
        val = self.curr_private.function[str(d)]
        result = []
        if val == 'e':
            for i in range(random.randint(1, 4)):
                result.append(random.choice(prvky['e']))
        else:
            val = self.curr_private.morphizms[p][int(val)]
            for v in val:
                result.append(random.choice(prvky[str(v)]))
        while result in used:
            rn = random.randint(0, len(result) - 1)
            result.insert(rn, random.choice(prvky['e']))
        return result

    def generate_subst(self):
        if self.h_card.get() == '' or self.morf_numb.get() == '':
            return
        if not (self.curr_private.morphizms and self.curr_private.function):
            messagebox.showerror('Error',
                                 'Najskôr vytvor morphizms a funkciu!')
        self.curr_public.substitutions = []
        used = []
        count = int(self.h_card.get())
        subst_count = int(self.morf_numb.get())
        prvky = self.curr_private.analyse_function()
        for p in range(subst_count):
            substitutions = []
            for d in range(count):
                subst = []
                for i in range(random.randint(1, 2)):
                    result = self.gen_subst(p, d, prvky, used)
                    subst.append(result)
                    used.append(result)
                substitutions.append(subst)
            self.curr_public.substitutions.append(substitutions)
        messagebox.showinfo('Success', 'Substitúcie úspešne vygenerované!')
