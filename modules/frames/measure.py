import tkinter as tk
import random
import time

import settings


class MeasureFrame(tk.Frame):

    def __init__(self, master):
        tk.Frame.__init__(self, master)
        self.active_frame = 'measure'
        self.salomaa_welzl = self.master.salomaa_welzl
        self.init_components()

    def init_components(self):
        self.master.geometry("")
        parametre = tk.LabelFrame(self, text="Parameters", bg=settings.BG, fg=settings.FG)
        parametre.grid(row=0, column=0, sticky='WE')

        tk.Label(parametre, text="Measures count:", bg=settings.BG, fg=settings.FG)\
            .grid(row=1, column=1, sticky='W')
        self.measures = tk.StringVar()
        self.measures_entry = tk.Entry(parametre, textvariable=self.measures)
        self.measures_entry.grid(row=1, column=2, sticky='WE')

        tk.Label(parametre, text="Plaintext bits size range:", bg=settings.BG, fg=settings.FG)\
            .grid(row=2, column=1, sticky='W')
        self.ot_size = tk.StringVar()
        self.ot_size_entry = tk.Entry(parametre, textvariable=self.ot_size)
        self.ot_size_entry.grid(row=2, column=2, sticky='WE')

        tk.Button(parametre, text="Measure!", command=lambda: self.measure())\
            .grid(row=3, column=1, sticky='WE')

        self.groups = []

    def gen_random_ot(self, dlzka, sustava=2):
        ot = ''
        for d in range(0, dlzka):
            ot += str(random.choice([0, 1]))
        return ot

    def measure(self):
        merania = {}
        tmp = self.ot_size.get()
        if '-' in tmp:
            rozsah = [int(x) for x in self.ot_size.get().split('-')]
        else:
            rozsah = [int(tmp), int(tmp)]
        for k in range(rozsah[0], rozsah[1]+1):
            meranie = {}
            meranie['dlzka_zt'] = []
            meranie['sifrovanie'] = []
            meranie['desifrovanie'] = []
            meranie['utok'] = []
            for j in range(int(self.measures.get())):
                ot = self.gen_random_ot(k)
                try:
                    start = time.time()
                    zt = self.salomaa_welzl.encrypt(ot)
                    end = time.time()
                    meranie['sifrovanie'].append(end - start)
                    meranie['dlzka_zt'].append(len(zt))
                except Exception:
                    continue

                try:
                    start = time.time()
                    self.salomaa_welzl.decrypt(','.join([str(x) for x in zt]))
                    end = time.time()
                    meranie['desifrovanie'].append(end - start)
                except Exception:
                    continue

                try:
                    start = time.time()
                    self.salomaa_welzl.attack(zt)
                    end = time.time()
                    meranie['utok'].append(end - start)
                except Exception:
                    continue
            merania[str(k)] = self.get_average(meranie)
        self.fill_values(merania)

    def get_average(self, merania):
        meranie_average = {}
        for key, value in merania.items():
            sum = 0
            for v in value:
                sum += v
            meranie_average[key] = sum / len(value)
        return meranie_average

    def fill_values(self, merania):
        for g in self.groups:
            g.grid_forget()
        self.groups = []

        c = 0

        for key, value in merania.items():
            self.groups.append(tk.LabelFrame(self))
            self.groups[c].grid(row=c + 1, column=0)
            tk.Message(self.groups[c], text=key, fg=settings.FG, bg=settings.BG)\
                .grid(row=c + 1, column=0, sticky="WE")
            tk.Message(self.groups[c], text=value['dlzka_zt'], fg=settings.FG, bg=settings.BG)\
                .grid(row=c + 1, column=1, sticky="WE")
            tk.Message(self.groups[c], text=value['sifrovanie'], fg=settings.FG, bg=settings.BG)\
                .grid(row=c + 1, column=2, sticky="WE")
            tk.Message(self.groups[c], text=value['desifrovanie'], fg=settings.FG, bg=settings.BG)\
                .grid(row=c + 1, column=3, sticky="WE")
            tk.Message(self.groups[c], text=value['utok'], fg=settings.FG, bg=settings.BG)\
                .grid(row=c + 1, column=4, sticky="WE")

            c += 1

        for x in self.winfo_children():
            x.configure(bg=settings.BG)
