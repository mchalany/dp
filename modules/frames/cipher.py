import tkinter as tk
from tkinter import messagebox
import time

import settings


class CipherFrame(tk.Frame):

    def __init__(self, master, typ):
        tk.Frame.__init__(self, master)
        self.master = master
        self.salomaa_welzl = master.salomaa_welzl
        self.typ = typ
        self.init_components()

    def init_components(self):
        self.master.geometry("350x350")
        if self.typ == 'c':
            self.master.active_frame = 'cipher'
            text = "Plaintext"
        elif self.typ == 'd':
            self.master.active_frame = 'decipher'
            text = "Ciphertext"
        tk.Label(self, text=text, bg=settings.BG, fg=settings.FG) \
            .grid(row=1, column=1, sticky='EW')
        self.text = tk.StringVar()
        self.text_entry = tk.Entry(self, textvariable=self.text)
        self.text_entry.grid(row=1, column=2, columnspan=2, sticky='EW')
        if self.typ == 'c':
            tk.Button(self, text="Cipher ▲", command=self.cipher)\
                .grid(row=2, column=2, sticky='EW')
            tk.Button(self, text="Export ▼", command=self.export)\
                .grid(row=2, column=3, sticky='EW')
        elif self.typ == 'd':
            tk.Button(self, text="Decipher ▲", command=self.decipher)\
                .grid(row=2, column=2, sticky='EW')
            tk.Button(self, text="Import ▼", command=self.load)\
                .grid(row=2, column=3, sticky='EW')
        tk.Button(self, text="Copy ▼", command=self.copy)\
            .grid(row=2, column=1, sticky='EW')
        self.after_label = tk.Message(self, text="nothing yet",
                                      fg=settings.FG, bg=settings.BG)
        self.after_label.grid(row=3, column=1, columnspan=3, sticky='EW')

    def load(self):
        try:
            with open('text.txt', 'r') as t:
                self.e = t.read()
            self.text_entry['text'] = 'Text loaded from the file...'
            messagebox.showinfo('Success',
                                'Ciphertext successfully imported!')
        except Exception:
            messagebox.showerror('Error',
                                 'Ciphertext could not have been imported!')

    def export(self):
        if self.e:
            data = ','.join(str(x) for x in self.e)
        else:
            data = ','.join(str(x) for x in self.text.get())
        try:
            with open('text.txt', 'w') as t:
                t.write(data)
            messagebox.showinfo('Success', 'Text successfully exported!')
        except Exception:
            messagebox.showerror('Error',
                                 'Text could not have been exported!')

    def cipher(self):
        start = time.time()
        self.e = self.salomaa_welzl.encrypt(self.text.get())
        end = time.time()
        messagebox.showinfo(
            'Done', 'Time: {} (length={})'.format(end-start, len(self.e)))
        if len(self.e) > 280:
            self.after_label['text'] = self.e[:280] + ['...']
            return
        self.after_label['text'] = self.e

    def decipher(self):
        start = time.time()
        try:
            if self.text_entry['text'] == 'Text loaded from the file...':
                self.after_label['text'] = self.salomaa_welzl.decrypt(self.e)
            else:
                self.after_label['text'] = \
                    self.salomaa_welzl.decrypt(self.text.get())
        except Exception:
            messagebox.showerror('Error', 'Text could not have been decrypted!')
            return
        end = time.time()
        messagebox.showinfo('Done', 'Time: {}'.format(end-start))

    def copy(self):
        try:
            import pyperclip
            if self.typ == 'c':
                pyperclip.copy(','.join(str(x) for x in self.e))
            elif self.typ == 'd':
                pyperclip.copy(','.join(str(x) for x in self.d))
            # pyperclip.copy(','.join(self.after_label['text'].split(' ')))
            messagebox.showinfo('Success', 'Text copied into the clipboard!')
        except ImportError:
            messagebox.showerror('Error',
                                 'Missing module pyperclip!')
