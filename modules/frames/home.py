import tkinter as tk
from tkinter import messagebox
import time

from modules.frames import automat
from modules.components.tree import Node

import settings


class HomeFrame(tk.Frame):

    def __init__(self, master):
        tk.Frame.__init__(self, master)
        self.master = master
        self.master.active_frame = 'home'
        self.solution_tree = Node()
        self.init_components()

    def init_components(self):
        self.configure(bg=settings.BG)
        self.master.geometry("700x500")

        crypto_settings = tk.LabelFrame(
            self, bg=settings.BG, fg=settings.FG, borderwidth=0)
        crypto_settings.grid(
            padx=(10, 10), pady=(10, 10), row=0, column=0, columnspan=2)

        tk.Label(crypto_settings, text="Enter ciphertext:",
                 bg=settings.BG, fg=settings.FG)\
            .grid(row=0, column=0, sticky='W')
        self.ct = tk.StringVar()
        ct_entry = tk.Entry(crypto_settings, textvariable=self.ct)
        ct_entry.grid(row=1, column=0, rowspan=2, sticky='we')

        tk.Button(crypto_settings, text="Start", command=lambda: self.start())\
            .grid(row=0, column=1, padx=(10, 10), pady=(0, 5), sticky="we")
        tk.Button(crypto_settings, text="Fast attack",
                  command=lambda: self.fast_attack())\
            .grid(row=1, column=1, padx=(10, 10), pady=(0, 0), sticky="we")

        tk.Label(crypto_settings, text="Plaintext bits: ",
                 bg=settings.BG, fg=settings.FG)\
            .grid(row=0, column=2, sticky='W')
        self.ot_label = tk.Label(crypto_settings, text="",
                                 bg=settings.BG, fg=settings.FG)
        self.ot_label.grid(row=1, column=2, sticky='W')

        self.groups = []

    def fast_attack(self):
        self.salomaa_welzl = self.master.salomaa_welzl
        try:
            ct = [int(x) for x in self.ct.get().split(',')]
            start = time.time()
            result = self.salomaa_welzl.attack(ct).find_node(
                self.salomaa_welzl.public.word).get_sigmas()
        except Exception:
            messagebox.showerror('Error', 'Cannot apply cryptanalysis on this '
                                 'ciphertext!')
            return
        arr = ''.join([str(x) for x in result if x is not None])
        end = time.time()
        messagebox.showinfo('Success', 'Found plaintext:' + str(arr) +
                            '\n(čas: ' + str(end-start) + ')')

    def start(self):
        try:
            self.solution_tree.data = \
                [int(x) for x in self.ct.get().split(',')]
        except Exception:
            messagebox.showerror('Error',
                                 'Incorrect format of ciphertext')
            return
        self.solution_tree.fill = "green"
        for g in self.groups:
            g.grid_forget()
        self.groups = []
        self.groups.append(tk.LabelFrame(self))
        self.groups[0].grid(row=1, column=0, columnspan=2)
        ct1 = tk.Message(self.groups[0], text=self.solution_tree.data,
                         width=600, fg=settings.FG, bg=settings.BG)
        ct1.grid(row=1, column=1, sticky="ew")
        ct1_button = tk.Button(self.groups[0], text="Automat", bg="blue", fg="white",
                               command=lambda: automat.AutomatFrame(
                                self.master, self.solution_tree, self))
        ct1_button.grid(row=1, column=2, sticky="ew")
        for x in self.winfo_children():
            x.configure(bg=settings.BG)

    def add(self, node):
        if node.data == self.master.salomaa_welzl.public.word:
            messagebox.showinfo('Success',
                                'Successfully decrypted plaintext bits: ' +
                                str(node.parent.sigma) + self.ot_label['text'])
        node.fill = "green"
        size = self.grid_size()
        if node.parent.parent is None:
            self.ot_label['text'] = str(node.parent.sigma)
        else:
            self.ot_label['text'] = (str(node.parent.sigma) +
                                     self.ot_label['text'])
        self.groups.append(tk.LabelFrame(self, bg=settings.BG, fg=settings.FG))
        self.groups[-1].grid(row=size[1], column=0, columnspan=2, sticky="ew")
        ct = tk.Message(self.groups[-1], text=node.data,
                        bg=settings.BG, fg=settings.FG, width=600)
        ct.grid(row=0, rowspan=2, column=0, sticky="w")
        ab = tk.Button(self.groups[-1], text="Automat", bg="blue", fg="white",
                       command=lambda: automat.AutomatFrame(
                        self.master, node, self))
        ab.grid(row=0, column=1, sticky="e")
        rb = tk.Button(self.groups[-1], text="Clear", bg="red", fg="white",
                       command=lambda: self.remove(size[1]))
        rb.grid(row=1, column=1, sticky="e")

    def remove(self, number):
        tex = [int(x) for x in
               self.groups[number-1].winfo_children()[0]['text'].split(' ')]
        node = self.solution_tree.find_node(tex)
        node.fill = "red"
        node.children = []
        self.groups[number-1].grid_remove()
        del self.groups[number-1]
