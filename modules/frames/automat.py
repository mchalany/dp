import tkinter as tk
from tkinter import messagebox

from utils.helpers import get_delta, get_graph, ScrollFrame
from modules.frames import graph

from modules.components import tree

import settings


class AutomatFrame(tk.Toplevel):

    def __init__(self, master, tree, home):
        tk.Toplevel.__init__(self)
        self.grab_set()

        self.master = master
        self.private = self.master.salomaa_welzl.private
        self.public = self.master.salomaa_welzl.public
        self.solution_tree = tree
        self.home = home
        self.node = self.solution_tree.find_node(tree.data)

        self.sigma = None

        self.init_components()
        self.render(tree.data)

    def init_components(self):
        self.resizable(False, False)
        self.next_button = tk.Button(self, text="Show graph!",
                                     command=lambda: self.next(),
                                     fg="white", bg="green")
        self.delta0_button = tk.Button(self, text="sigma0",
                                       command=lambda: self.apply_sigma(0))
        self.delta1_button = tk.Button(self, text="sigma1",
                                       command=lambda: self.apply_sigma(1))
        self.delta0_label = tk.Label(self, text="", bg=settings.BG)
        self.delta1_label = tk.Label(self, text="", bg=settings.BG)
        self.canvas = tk.Canvas(self, bg=settings.BG, width=470, height=440)
        self.delta0_button.grid(row=0, column=0, pady=10)
        self.delta0_label.grid(row=0, column=1, pady=10)
        self.next_button.grid(row=0, column=2, pady=10)
        self.delta1_label.grid(row=0, column=3, pady=10)
        self.delta1_button.grid(row=0, column=4, pady=10)

        self.canvas.grid(row=1, column=0, columnspan=5, sticky="news")
        atm_scrollbar = tk.Scrollbar(self, orient="vertical",
                                     command=self.canvas.yview)
        atm_scrollbar.grid(row=1, column=5, sticky="ns")
        self.canvas.config(yscrollcommand=atm_scrollbar.set, bg=settings.BG)
        self.configure(bg=settings.BG)

    def render(self, data):
        x = 40  # x-coord
        y = 40  # y-coord
        w = 30  # width
        o = 50  # offset
        self.states = []
        j = 7

        for i, d in enumerate(data):
            self.states.append(self.canvas.create_oval(
                x, y, x + w, y + w, fill="white", outline=settings.FG,
                tag=('circle'+str(i), 'circle')))
            self.canvas.create_text(x + w / 2, y + w / 2, text=i,
                                    fill=settings.FG,
                                    tag=('text'+str(i), 'text'))
            if j == i:
                self.canvas.create_line(x + w / 2, y + w,
                                        x + w / 2, y + w + 20,
                                        x + w / 2 - 5, y + w + 11,
                                        x + w / 2, y + w + 20,
                                        x + w / 2 + 5, y + w + 11,
                                        fill=settings.FG,
                                        tag=('line'+str(i), 'line'))
                if (j // 8) % 2 == 0:
                    self.canvas.create_text(x + w + 5, y + w + 10, text=d,
                                            font=settings.SMALL_FONT,
                                            fill=settings.FG)
                elif (j // 8) % 2 == 1:
                    self.canvas.create_text(x - 5, y + w + 10, text=d,
                                            font=settings.SMALL_FONT,
                                            fill=settings.FG)
                y += o
                j += 8
            elif (j // 8) % 2 == 0:
                self.canvas.create_line(x + w, y + w / 2, x + o, y + w / 2,
                                        x + o - 8, y + w / 2 - 5, x + o, y + w / 2,
                                        x + o - 8, y + w / 2 + 5,
                                        fill=settings.FG,
                                        tag=('line'+str(i), 'line'))
                self.canvas.create_text(x + w + 10, y, text=d,
                                        font=settings.SMALL_FONT,
                                        fill=settings.FG)
                x += o
            elif (j // 8) % 2 == 1:
                self.canvas.create_line(x, y + w / 2, x - 20, y + w / 2,
                                        x - 11, y + w / 2 - 5,
                                        x - 20, y + w / 2,
                                        x - 11, y + w / 2 + 5,
                                        fill=settings.FG,
                                        tag=('line'+str(i), 'line'))
                self.canvas.create_text(
                    x - 10, y, text=d, font=settings.SMALL_FONT,
                    fill=settings.FG)
                x -= o
        self.states.append(self.canvas.create_oval(
            x, y, x + w, y + w, fill="white", outline=settings.FG,
            tag=('circle'+str(i), 'circle')))
        self.canvas.create_text(
            x + w / 2, y + w / 2, text=i, fill=settings.FG,
            tag=('text'+str(i+1), 'text'))

        tmp = self.canvas.bbox("all")
        region = [tmp[0] - 25, tmp[1] - 25, tmp[2] + 20, tmp[3] + 20]
        self.canvas.config(scrollregion=region)

    def apply_sigma(self, sigma):
        self.delta0_label.config(text="")
        self.delta1_label.config(text="")
        for s in self.canvas.find_withtag('text'):
            self.canvas.tag_unbind(s, "<Button-1>")
        for s in self.canvas.find_withtag('circle'):
            self.canvas.tag_unbind(s, "<Button-1>")
            self.canvas.itemconfigure(s, fill="white", outline=settings.FG)
        for s in self.canvas.find_withtag('line'):
            self.canvas.itemconfigure(s, fill=settings.FG)
        if self.sigma == sigma:
            self.sigma = None
            return
        else:
            self.sigma = sigma
        sigma = self.public.substitutions[sigma]
        ct = self.solution_tree.data
        delta = get_delta(ct, sigma)
        for x in list(delta.keys())[:-1]:
            circle = self.canvas.find_withtag('circle'+str(x))[0]
            text = self.canvas.find_withtag('text'+str(x))[0]
            self.canvas.itemconfigure(circle, fill="red")
            self.canvas.tag_bind(
                text, "<Button-1>", lambda event, s=sigma, d=delta,
                x=x: self.show_popup(s, d, x))
            self.canvas.tag_bind(circle, "<Button-1>", lambda event, s=sigma,
                                 d=delta, x=x: self.show_popup(s, d, x))
            for i, d in enumerate(delta[x]):
                if i % 2 == 1:
                    for k in range(x, d):
                        self.canvas.itemconfigure(self.canvas.find_withtag(
                            'line'+str(k))[0], fill="red")
        if not get_graph(delta, []):
            text, font_color = 'X', 'red'
        else:
            text, font_color = 'OK', 'green'
        if self.sigma:
            self.delta1_label.config(text=text, fg=font_color)
        else:
            self.delta0_label.config(text=text, fg=font_color)

    def show_popup(self, sigma, delta, x):
        popup = ScrollFrame()
        popup.geometry("350x400")
        self.configure(bg=settings.BG)
        popup.resizable(False, False)
        popup.title('State '+str(x))

        group1 = tk.LabelFrame(popup.viewPort, text="Delta's", bg=settings.BG)
        group1.grid(row=1, column=1, columnspan=2, sticky="nsew")

        for d in range(int(len(delta[x]) / 2)):
            tk.Label(group1, text="from: ", bg=settings.BG, fg=settings.FG) \
                .grid(row=d*4+1, column=1)
            tk.Label(group1, text=x, bg=settings.BG, fg=settings.FG) \
                .grid(row=d*4+1, column=2)
            tk.Label(group1, text="to: ", bg=settings.BG, fg=settings.FG) \
                .grid(row=d*4+1, column=3)
            tk.Label(group1, text=delta[x][d*2+1], bg=settings.BG,
                     fg=settings.FG).grid(row=d*4+1, column=4)

            tk.Label(group1, text="used subst: ", bg=settings.BG,
                     fg=settings.FG).grid(row=d*4+2, column=1, columnspan=2)
            tk.Label(group1, text=sigma[delta[x][d*2]], bg=settings.BG,
                     fg=settings.FG).grid(row=d*4+2, column=3, columnspan=2)

            tk.Label(group1, text=self.solution_tree.data[x], bg=settings.BG,
                     fg=settings.FG).grid(row=d*4+3, column=1)
            tk.Label(group1, text=" -> ", bg=settings.BG, fg=settings.FG) \
                .grid(row=d*4+3, column=2)
            tk.Label(group1, text=delta[x][d*2], bg=settings.BG,
                     fg=settings.FG).grid(row=d*4+3, column=3)
            tk.Label(group1, text="", bg=settings.BG, fg=settings.FG) \
                .grid(row=d*4+3, column=4)

        group2 = tk.LabelFrame(popup.viewPort, text="Substitution",
                               bg=settings.BG, fg=settings.FG)
        group2.grid(row=2, column=1, sticky="nsew")
        sigma_list = ''
        for i in self.public.abeceda:
            sigma_list += str(i) + ' -> ' + \
                ', '.join(str(s) for s in sigma[i]) + '\n'
        tk.Message(group2, text=sigma_list, width=150, bg=settings.BG,
                   fg=settings.FG).grid(row=1, column=1)

        group3 = tk.LabelFrame(popup.viewPort, text="Delta function",
                               bg=settings.BG, fg=settings.FG)
        group3.grid(row=2, column=2, sticky="nsew")
        delta_list = ''
        for i in delta:
            delta_list += str(i) + ' -> ' + \
                ', '.join(str(s) for s in delta[i]) + '\n'
        tk.Message(group3, text=delta_list, width=150, bg=settings.BG,
                   fg=settings.FG).grid(row=1, column=1)

    def next(self):
        if self.sigma is None:
            messagebox.showerror('Error', 'Choose the substitution!')
            return
        sigma = self.public.substitutions[self.sigma]
        delta = get_delta(self.node.data, sigma)
        _list = get_graph(delta, [])
        if not _list:
            messagebox.showerror('Error', 'Incorrect substitution!')
            return
        else:
            self.node.sigma = self.sigma
        if not self.node.children:
            for z in _list:
                new_child = tree.Node()
                new_child.data = z
                new_child.parent = self.node
                self.node.add_child(new_child)
        self.destroy()
        graph.GraphFrame(self.master, self.home, self.node)
