import tkinter as tk

from modules.frames import parameters


class FunctionFrame(tk.Frame):

    def __init__(self, master, parent):
        tk.Frame.__init__(self, master)
        self.pack(fill=tk.BOTH, expand=True)
        self.parent = parent
        self.private = parent.curr_private
        self.public = parent.curr_public
        self.textvars = []
        self.entries = []
        self.init_components()
        self.init_model()

    def init_components(self):
        tk.Button(self, text="Clear", command=lambda: self.erase())\
            .grid(sticky='ew', row=1, column=1)
        tk.Button(self, text="OK", command=lambda: self.confirm())\
            .grid(sticky='ew', row=1, column=2)
        for h in range(2, self.public.char_count + 2):
            tk.Label(self, text=h-2).grid(row=h, column=1)
            self.textvars.append(tk.StringVar())
            self.entries.append(tk.Entry(self, textvariable=self.textvars[-1]))
            self.entries[-1].grid(row=h, column=2, columnspan=2, sticky='WE')
            self.columnconfigure(2, weight=1)

    def init_model(self):
        for key, value in self.private.function.items():
            self.textvars[int(key)].set(value)

    def erase(self):
        for h in range(self.public.char_count):
            self.entries[h].delete(0, tk.END)

    def confirm(self):
        function = {}
        for h in range(self.public.char_count):
            try:
                function[str(h)] = int(self.textvars[h].get())
            except Exception:
                function[str(h)] = self.textvars[h].get()
        self.private.function = function
        data = [self.private, self.public]
        self.master.switch_frame(parameters.ParametersFrame, data)
