import tkinter as tk
from tkinter import messagebox

from utils.helpers import get_delta, get_graph

import settings


class GraphFrame(tk.Toplevel):

    def __init__(self, master, main, node):
        tk.Toplevel.__init__(self)
        self.grab_set()

        self.master = master
        self.main = main
        self.node = node

        self.configure(bg=settings.BG)

        self.helped = 0
        self.init_components()
        self.init_tree()
        self.master.update()
        self.main.solution_tree.render(self.r, self.canvas)
        self.bind_tree(self.main.solution_tree)

    def init_components(self):
        self.resizable(False, False)

        tk.Label(self, text="Substitution: ",
                 bg=settings.BG, fg=settings.FG, anchor="w") \
            .grid(row=1, column=1, sticky="ew")
        self.subst = tk.Label(self, text="please, choose one of the tree lists",
                              bg=settings.BG, fg=settings.FG, anchor="w")
        self.subst.grid(row=1, column=2, sticky="ew")

        self.use_button = tk.Button(
            self, text="Use", anchor="e", bg="blue", fg="white")
        self.use_button.grid(row=1, column=3)

        self.help_button = tk.Button(
            self, text="Help", anchor="e", bg="blue", fg="white",
            command=lambda: self.help(self.node))
        self.help_button.grid(row=1, column=4)

        self.canvas = tk.Canvas(self, bg=settings.BG, width=500, height=250)
        self.canvas.grid(row=2, column=1, columnspan=4, sticky="ew")
        tree_vscrollbar = tk.Scrollbar(self, orient="vertical",
                                       command=self.canvas.yview)
        tree_vscrollbar.grid(row=2, column=5, sticky="ns")
        tree_hscrollbar = tk.Scrollbar(self, orient="horizontal",
                                       command=self.canvas.xview)
        tree_hscrollbar.grid(row=3, column=1, columnspan=4, sticky="we")
        self.canvas.config(yscrollcommand=tree_vscrollbar.set,
                           xscrollcommand=tree_hscrollbar.set,
                           bg=settings.BG)

        self.ct_text = tk.Message(
            self, text="Start by clicking on one of the tree lists...",
            width=500, bg=settings.BG, fg=settings.FG, anchor="nw")
        self.ct_text.grid(row=4, column=1, columnspan=4, sticky="nsew")

    def init_tree(self):
        levels, cols, rows = self.main.solution_tree.get_levels({}, 0)
        r = 30
        self.r = r
        s_x = 0
        s_y = 0
        node = list(levels.values())[0][0]
        node.circle_pos = (s_x, s_y)
        node.line_from = (s_x + r / 2, s_y + r)
        for m, l in enumerate(list(levels.values())[1:]):
            y = s_y + r * 3 * (m + 1)
            if len(l) % 2 == 0:
                x = [s_x - r / 2 - 5, s_x + r / 2 + 5]
                for i in range(int(((len(l)-2) / 2))):
                    i += 1
                    x.insert(0, s_x - r / 2 - 5 - i * (r + 10))
                    x.insert(len(x) - 1, s_x + r / 2 + 5 + i * (r + 10))
            else:
                x = [s_x]
                for i in range(int((len(l) - 1 / 2))):
                    x.insert(0, s_x - i * (r / 2 + 10))
                    x.insert(len(x) - 1, s_x + i * (r / 2 + 10))
            for j, node in enumerate(l):
                node.circle_pos = (x[j], y)
                node.line_to = (x[j] + r / 2, y)
                node.line_from = (x[j] + r / 2, y + r)

    def bind_tree(self, node):
        self.canvas.tag_bind(node.circle, "<Button-1>",
                             lambda event, node=node: self.node_info(node))
        for x in node.children:
            self.bind_tree(x)

    def deselect_all(self, node):
        f = self.canvas.itemcget(node.circle, 'outline')
        self.canvas.itemconfigure(node.circle, fill=f)
        for x in node.children:
            self.deselect_all(x)
        self.ct_text['text'] = "Start by clicking on one of the lists..."
        self.subst['text'] = "choose list"
        self.use_button['command'] = ""

    def deselect(self, node):
        f = self.canvas.itemcget(node.circle, 'outline')
        self.canvas.itemconfigure(node.circle, fill=f)
        self.ct_text['text'] = "Start by clicking on one of the lists..."
        self.subst['text'] = "choose list"
        self.use_button['command'] = ""

    def check_existing(self, node):
        for x in self.main.groups:
            t = [int(x) for x in x.winfo_children()[0]['text'].split(' ')]
            if t == node.data:
                return True
        return False

    def node_info(self, node):
        if self.check_existing(node):
            messagebox.showerror('Error',
                                 'This string is already among the items!')
            return
        if self.canvas.itemcget(node.circle, 'fill') == "white":
            self.deselect(node)
        else:
            self.deselect_all(self.main.solution_tree)
            self.canvas.itemconfigure(node.circle, fill="white")
            self.ct_text['text'] = node.data
            if node.parent is not None:
                self.subst['text'] = node.parent.sigma
            self.use_button['command'] = \
                lambda: (self.destroy(), self.main.add(node))

    def help(self, node):
        if not self.helped:
            self.deselect_all(self.main.solution_tree)
            for x in node.children:
                z = []
                for i in self.master.salomaa_welzl.public.substitutions:
                    d = get_delta(x.data, i)
                    z.append(bool(get_graph(d, [])))
                if True not in z:
                    self.canvas.itemconfigure(
                        x.circle, fill="red", outline="red")
            self.helped = 1
