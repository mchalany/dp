import tkinter as tk
from tkinter import messagebox

from modules.frames import parameters


class SubstFrame(tk.Frame):

    def __init__(self, master, parent):
        tk.Frame.__init__(self)
        self.private = parent.curr_private
        self.public = parent.curr_public
        self.check_subst_count = 0
        self.init_components()

    def init_components(self):
        self.textvars = []
        self.entries = []
        for p in range(self.public.char_count):
            tk.Label(self, text=p).grid(row=p, column=1)
            self.textvars.append(tk.StringVar())
            self.entries.append(tk.Entry(self, textvariable=self.textvars[-1]))
            self.entries[-1].grid(row=p, column=2, columnspan=2, sticky='e')
        tk.Button(self, text="Next", command=lambda: self.next())\
            .grid(sticky='ew', row=p+1, column=2, columnspan=2)
        self.init_model(self.check_subst_count)

    def init_model(self, current):
        if len(self.public.substitutions) - 1 >= current:
            for i, value in enumerate(self.public.substitutions[current]):
                if len(value) > 1:
                    tmp = ''
                    for v in value[:-1]:
                        tmp += ','.join([str(x) for x in v]) + ';'
                    tmp += ','.join([str(x) for x in value[-1]])
                    self.textvars[int(i)].set(tmp)
                else:
                    value = ','.join([str(x) for x in value[0]])
                    self.textvars[int(i)].set(value)

    def next(self):
        substitution = []
        for p in range(self.public.char_count):
            temp = self.textvars[p].get().split(';')
            tmp = []
            for t in temp:
                tmp.append([int(x) for x in t.split(',') if x != ''])
            substitution.append(tmp)
            self.entries[p].delete(0, tk.END)
        if len(self.public.substitutions) - 1 > self.check_subst_count:
            self.public.substitutions[self.check_subst_count] = substitution
        else:
            self.public.substitutions.append(substitution)
        self.check_subst_count += 1
        self.init_model(self.check_subst_count)
        if self.check_subst_count == self.public.subst_count:
            messagebox.showinfo('Success',
                                'All substitutions successfully created!')
            data = [self.private, self.public]
            self.master.switch_frame(parameters.ParametersFrame, data)
