from modules.components import tree
import tkinter as tk


def get_last_value(zt, zobraz):
    possible = []
    for h in zobraz:
        if zt[-len(h):] == h:
            possible.append(h)
    return max(possible, key=len)


def is_subst(ct, pos, subst):
    if len(ct) - pos < len(subst):
        return None
    for i, s in enumerate(subst):
        if ct[pos+i] != s:
            return None
    return pos+len(subst)


def get_delta(ct, sigma):
    delta = {}
    for i, s in enumerate(sigma):
        for x in s:
            for start, char in enumerate(ct):
                if x[0] == char:
                    if is_subst(ct, start, x):
                        if start in delta:
                            delta[start].extend([i, start+len(x)])
                        else:
                            delta[start] = [i, start+len(x)]
    sorted_delta = {}
    for keys in sorted(delta.keys()):
        sorted_delta[keys] = delta[keys]
    last = len(ct)
    sorted_delta[last] = [last]
    return sorted_delta


def get_graph(delta, _list, parent=None, key=0):
    graph = tree.Node()
    graph.parent = parent
    if key not in delta:
        return None
    if key == delta[list(delta.keys())[-1]][-1]:
        _list.append(graph.get_ancestors())
    else:
        for i in range(int(len(delta[key])/2)):
            graph.data = delta[key][i*2]
            graph.add_child(get_graph(delta, _list, graph, delta[key][i*2+1]))
    return _list


# from https://gist.github.com/mp035/9f2027c3ef9172264532fcd6262f3b01
class ScrollFrame(tk.Toplevel):
    def __init__(self):
        super().__init__()  # create a frame (self)

        # place canvas on self
        self.canvas = tk.Canvas(self, borderwidth=0, background="#ffffff")
        # place a frame on the canvas, this frame will hold the child widgets
        self.viewPort = tk.Frame(self.canvas, background="#ffffff")
        # place a scrollbar on self
        self.vsb = tk.Scrollbar(
            self, orient="vertical", command=self.canvas.yview)
        # attach scrollbar action to scroll of canvas
        self.canvas.configure(yscrollcommand=self.vsb.set)

        # pack scrollbar to right of self
        self.vsb.pack(side="right", fill="y")
        # pack canvas to left of self and expand to fil
        self.canvas.pack(side="left", fill="both", expand=True)
        # add view port frame to canvas
        self.canvas.create_window((4, 4), window=self.viewPort, anchor="nw",
                                  tags="self.viewPort")

        # bind an event whenever the size of the viewPort frame changes.
        self.viewPort.bind("<Configure>", self.onFrameConfigure)

    def onFrameConfigure(self, event):
        '''Reset the scroll region to encompass the inner frame'''
        self.canvas.configure(scrollregion=self.canvas.bbox("all"))
