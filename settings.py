APP_TITLE = 'SalomaaWelzl'

PUBLIC_KEY_PATH = 'misc/keys/1/public.json'
PRIVATE_KEY_PATH = 'misc/keys/1/private.json'

SMALL_FONT = ("Verdana", 8)
NORMAL_FONT = ("Verdana", 10)
BIG_FONT = ("Verdana", 15)

# BG = '#abcdef'
BG = '#ffffff'
FG = '#000000'

BG_MENU = ''
FG_MENU = ''

# MAX POCET SLOV V OBRAZE ZOBRAZENI
PSO = 3